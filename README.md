# Model Checker for LAST
RLAST is a recursive modal logic developed to check properties on abstract syntax trees for the extended lambda calculus.
This code is an implementation of RLASTS syntax and semantics, to assert whether a given property is satisfied by an abstract syntax tree.

RLAST's Syntax:
```
φ∶∶= b | r |¬φ | φ1∧φ2 | ⟨l⟩φ
b∶∶= tt| const | var | hole
r∶∶= X | min(X,φ)
```
