{-# LANGUAGE UnicodeSyntax #-}

module RLAST.Denotation where

import qualified Data.Map as M
import Data.Maybe (fromJust)
import Data.Set as S
import Data.Set.Unicode
import RLAST.Formula as F
import RLAST.Language as La
import RLAST.Lts
import Data.Function.Extra (($:))

computeDenotations :: Lts -> Formula -> M.Map X (Set A) -> Set A
computeDenotations lts@(s, l, t) φ ρ = case φ of
  Tt -> s
  Negation φ' -> s ∖ computeDenotations lts φ' ρ
  Or φ₁ φ₂ -> computeDenotations lts φ₁ ρ ∪ computeDenotations lts φ₂ ρ
  Must lb φ' ->
    fromList
      [ a
        | a <- toList s,
          a' <- toList $ computeDenotations lts φ' ρ,
          (a, lb, a') ∈ t
      ]
  F.Hole -> singleton La.Hole
  F.Const -> fromList [s' | s' <- toList s, k s']
    where
      k (La.Const _) = True
      k _ = False
  F.Var -> fromList [s' | s' <- toList s, k s']
    where
      k (La.Var _) = True
      k _ = False
  Min x φ' -> fixpoint x (∅) $: computeDenotations lts φ' $: ρ
  RecVar x -> fromJust $ M.lookup x ρ -- Crashes if the variable isn't in the environment ρ

fixpoint :: X -> Set A -> (M.Map X (Set A) -> Set A) -> M.Map X (Set A) -> Set A
fixpoint x e f ρ = if e' ⊆ e then e' else fixpoint x e' f ρ
  where
    e' = f $ M.insert x e ρ
