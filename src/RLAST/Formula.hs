module RLAST.Formula where

import RLAST.Label

data Formula
  = Tt
  | Hole
  | Const
  | Var
  | Negation Formula
  | Or Formula Formula
  | Must Label Formula
  | RecVar String
  | Min String Formula
  deriving (Ord, Eq, Show)
