module RLAST.Label where

data Label
  = Apply₁
  | Apply₂
  | Lambda₁
  | Lambda₂
  | Cursor
  | Break
  deriving (Ord, Eq, Show, Enum, Bounded)
