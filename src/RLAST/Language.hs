module RLAST.Language where

type X = String

type C = Integer

data A
  = Var X
  | Const C
  | Lambda X A
  | Apply A A
  | Cursor A
  | Break A
  | Hole
  deriving (Eq, Show, Ord)
