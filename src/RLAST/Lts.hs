{-# LANGUAGE UnicodeSyntax #-}

module RLAST.Lts where

import Data.Set as S
import Data.Set.Unicode
import RLAST.Label as Lb
import RLAST.Language as La

type Lts = (Set A, Set Label, Set (A, Label, A))

makeLts :: A -> Lts
makeLts a = case a of
  x@(Var _) -> (singleton x, (∅), (∅))
  c@(Const _) -> (singleton c, (∅), (∅))
  Hole -> (singleton a, (∅), (∅))
  Apply a₁ a₂ -> combine3Lts (sₐ, lₐ, tₐ) ltsₐ₁ ltsₐ₂
    where
      sₐ = singleton a
      lₐ = fromList [Apply₁, Apply₂]
      tₐ = fromList [(a, Apply₁, a₁), (a, Apply₂, a₂)]
      ltsₐ₁ = makeLts a₁
      ltsₐ₂ = makeLts a₂
  Lambda x a₂ -> combine3Lts (sₐ, lₐ, tₐ) ltsₐ₁ ltsₐ₂
    where
      sₐ = singleton a
      lₐ = fromList [Lambda₁, Lambda₂]
      tₐ = fromList [(a, Lambda₁, Var x), (a, Lambda₂, a₂)]
      ltsₐ₁ = makeLts $ Var x
      ltsₐ₂ = makeLts a₂
  La.Cursor a' -> combineLts (sₐ, lₐ, tₐ) ltsₐ'
    where
      sₐ = singleton a
      lₐ = singleton Lb.Cursor
      tₐ = singleton (a, Lb.Cursor, a')
      ltsₐ' = makeLts a'
  La.Break a' -> combineLts (sₐ, lₐ, tₐ) ltsₐ'
    where
      sₐ = singleton a
      lₐ = singleton Lb.Break
      tₐ = singleton (a, Lb.Break, a')
      ltsₐ' = makeLts a'

combine3Lts :: Lts -> Lts -> Lts -> Lts
combine3Lts lts₁ lts₂ lts₃ = combineLts lts₁ $ combineLts lts₂ lts₃

combineLts :: Lts -> Lts -> Lts
combineLts (s₁, l₁, t₁) (s₂, l₂, t₂) = (s₁ ∪ s₂, l₁ ∪ l₂, t₁ ∪ t₂)
