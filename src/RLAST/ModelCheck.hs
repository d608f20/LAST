{-# LANGUAGE UnicodeSyntax #-}

module RLAST.ModelCheck where

import Data.Map as M
import Data.Set as S
import Data.Set.Unicode
import RLAST.Denotation
import RLAST.Formula
import RLAST.Language
import RLAST.Lts

modelCheck a φ = a ∈ computeDenotations lts φ M.empty
  where
    lts = makeLts a

satisfies = modelCheck

(⊨) = satisfies
