module RLAST.SugurizedFormula where

import Data.Function.Extra (($:))
import qualified RLAST.Formula as F
import RLAST.Label

data SugurizedFormula
  = Tt
  | Ff
  | Hole
  | Const
  | Var
  | Negation SugurizedFormula
  | Or SugurizedFormula SugurizedFormula
  | And SugurizedFormula SugurizedFormula
  | Must Label SugurizedFormula
  | MustL [Label] SugurizedFormula
  | MustA SugurizedFormula
  | Every Label SugurizedFormula
  | EveryL [Label] SugurizedFormula
  | EveryA SugurizedFormula
  | RecVar String
  | Min String SugurizedFormula

toFormula φ = case φ of
  Tt -> F.Tt
  Ff -> F.Negation F.Tt
  Hole -> F.Hole
  Const -> F.Const
  Var -> F.Var
  Negation φ' -> F.Negation $ toFormula φ'
  Or φ₁ φ₂ -> F.Or $: toFormula φ₁ $: toFormula φ₂
  And φ₁ φ₂ ->
    F.Negation $
      F.Or
        (F.Negation $ toFormula φ₁)
        (F.Negation $ toFormula φ₂)
  Must l φ' -> F.Must l $ toFormula φ'
  MustL l φ' ->
    foldl (\x y -> F.Or x $ toFormula $ Must y φ') $: toFormula Ff $: l
  MustA φ' -> toFormula $ MustL [minBound .. maxBound] φ'
  Every l φ' ->
    F.Negation
      $ F.Must l
      $ F.Negation
      $ toFormula φ'
  EveryL l φ' ->
    toFormula $ foldl (\x y -> And x $ Every y φ') Tt l
  EveryA φ' -> toFormula $ EveryL [minBound .. maxBound] φ'
  RecVar s -> F.RecVar s
  Min s φ' -> F.Min s $ toFormula φ'
