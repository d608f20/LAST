{-# LANGUAGE UnicodeSyntax #-}

module RLAST.DenotationSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Data.Set as S
import Data.Set.Unicode
import RLAST.Denotation
import RLAST.Formula as F
import RLAST.Language as La
import Test.Hspec

spec :: Spec
spec = do
  describe "computeDenotations" $ do
    it "should return the same state set given the formula tt" $ do
      let a = singleton (La.Const 3)
      let φ = Tt
      computeDenotations (a, (∅), (∅)) φ M.empty `shouldBe` a
    it "should return the empty set given the formula -tt" $ do
      let a = singleton (La.Const 3)
      let φ = Negation Tt
      computeDenotations (a, (∅), (∅)) φ M.empty `shouldBe` (∅)
