{-# LANGUAGE UnicodeSyntax #-}

module RLAST.LtsSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Data.Set as S
import Data.Set.Unicode
import RLAST.Lts
import RLAST.Formula as F
import RLAST.Language as La
import Test.Hspec

spec :: Spec
spec = do
  describe "makeLts" $ do
    it "should return the LTS with state Hole, no labels and no transitions given a Hole return" $ do
      let a = La.Hole
      makeLts a `shouldBe` (singleton a, (∅), (∅))
