{-# LANGUAGE UnicodeSyntax #-}

module RLAST.ModelCheckSpec
  ( spec,
  )
where

import qualified Data.Map as M
import Data.Set as S
import Data.Set.Unicode
import RLAST.Denotation
import RLAST.Language as La
import RLAST.Lts
import RLAST.ModelCheck
import RLAST.SugurizedFormula as SF
import Test.Hspec

spec :: Spec
spec = do
  describe "satisfies" $ do
    it "should not satisfy a ast with a hole given the formula min(x, -hole and [*]x)" $ do
      let a = Apply (Lambda "a" (La.Const 1)) (La.Hole)
      let φ = toFormula $ Min "X" (And (Negation SF.Hole) (EveryA (RecVar "X")))
      a ⊨ φ `shouldBe` False
    it "should satisfy a ast without a hole given the formula min(x, -hole and [*]x)" $ do
      let a = Apply (Lambda "a" (La.Const 1)) (La.Var "a")
      let φ = toFormula $ Min "X" (And (Negation SF.Hole) (EveryA (RecVar "X")))
      a ⊨ φ `shouldBe` True
